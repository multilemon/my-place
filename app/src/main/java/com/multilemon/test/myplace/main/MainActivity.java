package com.multilemon.test.myplace.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.multilemon.test.myplace.R;
import com.multilemon.test.myplace.common.PlaceCardView;
import com.multilemon.test.myplace.model.MyPlace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, PlaceCardView.PlaceCardViewInterface {

    private static final int MAX_RESULT = 10;

    @Bind(R.id.tab_layout)
    TabLayout mTabLayout;
    @Bind(R.id.main_viewpager)
    ViewPager mMainViewPager;

    TabsAdapter mTabsAdapter;
    private double latitude, longitude;
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<MyPlace> mMyPlaceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mTabsAdapter = new TabsAdapter(getSupportFragmentManager());
        mMainViewPager.setAdapter(mTabsAdapter);

        mTabLayout.setSelectedTabIndicatorColor(getResources().getColor(android.R.color.white));
        mTabLayout.setupWithViewPager(mMainViewPager);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

//    @Override
//    public void onStop() {
//        mGoogleApiClient.disconnect();
//        super.onStop();
//    }

//    public void setTargetLocation(double latitude, double longitude) {
//        this.latitude = latitude;
//        this.longitude = longitude;
//    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void searchNearbyPlacesWithinRange(double latitude, double longitude, int radius) {
        this.latitude = latitude;
        this.longitude = longitude;
        String searchUrl = getString(R.string.search_url, latitude, longitude, radius, getString(R.string.google_maps_browser_key));

        OkHttpClient client = new OkHttpClient();
        Request.Builder builder = new Request.Builder();
        Request request = builder.url(searchUrl).build();
        client.newCall(request).enqueue(new SearchNearbyCallback());
    }

    private class SearchNearbyCallback implements Callback {

        @Override
        public void onFailure(Request request, IOException e) {
            Log.e("MainActivity", "SearchNearbyCallback onFailure: " + e.getMessage());
//            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onResponse(Response response) throws IOException {
            try {
                String tmp = response.body().string();
                JSONObject responseJSONObject = new JSONObject(Html.fromHtml(tmp).toString());
                JSONArray placesJSONArray = responseJSONObject.getJSONArray("results");

                int count = placesJSONArray.length() < MAX_RESULT ? placesJSONArray.length() : MAX_RESULT;
                String[] placeIds = new String[count];
                for (int i = 0; i < count; i++) {
                    placeIds[i] = placesJSONArray.getJSONObject(i).getString("place_id");
                }

                final ArrayList<MyPlace> myPlaceList = new ArrayList<>();

                PendingResult<PlaceBuffer> pendingResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeIds);
                pendingResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceBuffer places) {
                        if (places.getCount() > 0) {
                            for (int i = 0; i < places.getCount(); i++) {
                                Place place = places.get(i);
                                MyPlace myPlace = new MyPlace(place.getId(), place.getName().toString(), place.getAddress().toString(), place.getWebsiteUri());
                                myPlace.retrievePlacePhoto(mGoogleApiClient);
                                myPlaceList.add(myPlace);
                            }
                            places.release();
                            setDataSet(myPlaceList);
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setDataSet(ArrayList<MyPlace> myPlaceList) {
        this.mMyPlaceList = myPlaceList;
        ((PlacesFragment) mTabsAdapter.getItem(0)).setDataSet(mMyPlaceList);
        ((FavoritesFragment) mTabsAdapter.getItem(1)).setDataSet(mMyPlaceList);
    }

    private void notifyItemChanged(int position) {
        ((PlacesFragment) mTabsAdapter.getItem(0)).notifyItemChanged(position);
        ((FavoritesFragment) mTabsAdapter.getItem(1)).notifyItemChanged(position);

    }

    public class TabsAdapter extends FragmentStatePagerAdapter {

        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return PlacesFragment.newInstance(MainActivity.this);
                case 1:
                    return FavoritesFragment.newInstance(MainActivity.this);
                default:
                    throw new UnsupportedOperationException();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.places_tab_title);
                case 1:
                    return getString(R.string.favorites_tab_title);
                default:
                    return "";
            }
        }
    }

    @Override
    public void OnPlaceFavorite(MyPlace myPlace) {
        if (mMyPlaceList != null && !mMyPlaceList.isEmpty()) {
            int index = this.mMyPlaceList.indexOf(myPlace);
            if (index >= 0) {
                mMyPlaceList.get(index).setFavorite(true);
                notifyItemChanged(index);
            }
        }
    }

    @Override
    public void OnPlaceUnFavorite(MyPlace myPlace) {
        if (mMyPlaceList != null && !mMyPlaceList.isEmpty()) {
            int index = this.mMyPlaceList.indexOf(myPlace);
            if (index >= 0) {
                mMyPlaceList.get(index).setFavorite(false);
                notifyItemChanged(index);
            }
        }
    }
}
