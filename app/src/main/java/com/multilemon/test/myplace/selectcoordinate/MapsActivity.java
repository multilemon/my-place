package com.multilemon.test.myplace.selectcoordinate;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.multilemon.test.myplace.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnCameraChangeListener, View.OnClickListener {

    private static final int LOCATION_ACCESS_REQUEST_CODE = 2;
    private GoogleMap mMap;

    @Bind(R.id.current_coordinate_textview)
    TextView mCurrentCoordinateTextView;
    @Bind(R.id.select_this_point_button)
    Button mSelectThisPointButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mSelectThisPointButton.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker at Cyber World Tower and move the camera
        LatLng cyberworld = new LatLng(13.769415, 100.573664);
        mMap.addMarker(new MarkerOptions().position(cyberworld).title("Default Marker at Cyber World Tower"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cyberworld, 15.07f));
        mMap.setOnCameraChangeListener(this);

        checkLocationPermission();
    }

    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_ACCESS_REQUEST_CODE);
        } else {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_ACCESS_REQUEST_CODE && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkLocationPermission();
            }
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        mCurrentCoordinateTextView.setText(getString(R.string.latlng_format, cameraPosition.target.latitude, cameraPosition.target.longitude));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mSelectThisPointButton.getId()) {
            // TODO: set result and finish
            Intent intent = new Intent();
            intent.putExtra(getString(R.string.latitude_double_key), mMap.getCameraPosition().target.latitude);
            intent.putExtra(getString(R.string.longitude_double_key), mMap.getCameraPosition().target.longitude);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
