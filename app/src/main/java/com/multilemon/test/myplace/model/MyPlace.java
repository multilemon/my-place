package com.multilemon.test.myplace.model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;
import com.multilemon.test.myplace.common.PlaceCardView;

/**
 * Created by MultiLemon on 1/18/16.
 */
public class MyPlace {

    private String placeId;
    private String name;
    private String address;
    private Uri websiteUri;
    private Bitmap photo;
    private boolean isFavorite;
    private View view;

    public MyPlace() {
        placeId = "";
        name = "";
        address = "";
        websiteUri = null;
        photo = null;
        isFavorite = false;
    }

    public MyPlace(String placeId, String name, String address, Uri websiteUri) {
        this.placeId = placeId;
        this.name = name;
        this.address = address;
        this.websiteUri = websiteUri;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Uri getWebsiteUri() {
        return websiteUri;
    }

    public void setWebsiteUri(Uri websiteUri) {
        this.websiteUri = websiteUri;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }


    public void setView(View view) {
        this.view = view;
    }

    public void retrievePlacePhoto(GoogleApiClient googleApiClient) {
        new GetPlacePhoto(googleApiClient).execute();
    }

    private class GetPlacePhoto extends AsyncTask<String, Integer, Bitmap> {

        GoogleApiClient mGoogleApiClient;

        public GetPlacePhoto(GoogleApiClient googleApiClient) {
            this.mGoogleApiClient = googleApiClient;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap placePhoto = null;

            if (mGoogleApiClient.isConnected()) {
                PlacePhotoMetadataResult result = Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, placeId).await();
                if (result.getStatus().isSuccess()) {
                    PlacePhotoMetadataBuffer placePhotoMetadataBuffer = result.getPhotoMetadata();
                    if (placePhotoMetadataBuffer.getCount() > 0 && !isCancelled()) {
                        PlacePhotoMetadata placePhotoMetadata = placePhotoMetadataBuffer.get(0);
                        placePhoto = placePhotoMetadata.getScaledPhoto(mGoogleApiClient, 400, 400).await().getBitmap();
                    }
                    placePhotoMetadataBuffer.release();
                }

            }

            return placePhoto;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            photo = bitmap;
            if (view != null) {
                ((PlaceCardView) view).setPhoto(photo);
            }
        }
    }

}
