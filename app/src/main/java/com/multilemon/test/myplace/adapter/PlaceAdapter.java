package com.multilemon.test.myplace.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.multilemon.test.myplace.R;
import com.multilemon.test.myplace.common.PlaceCardView;
import com.multilemon.test.myplace.model.MyPlace;
import com.multilemon.test.myplace.viewholder.PlaceViewHolder;

import java.util.ArrayList;

/**
 * Created by MultiLemon on 1/13/16.
 */
public class PlaceAdapter extends RecyclerView.Adapter<PlaceViewHolder> {

    private ArrayList<MyPlace> mMyPlaceList;
    private int mCount;
    private PlaceCardView.PlaceCardViewInterface mListener;

    public PlaceAdapter(PlaceCardView.PlaceCardViewInterface listener) {
        this.mMyPlaceList = new ArrayList<>();
        this.mCount = 0;
        this.mListener = listener;
    }

    public PlaceAdapter(PlaceCardView.PlaceCardViewInterface listener, ArrayList<MyPlace> myPlaceList) {
        this.mMyPlaceList = myPlaceList;
        this.mCount = mMyPlaceList.size();
        this.mListener = listener;
    }

    public void setPlaceList(ArrayList<MyPlace> myPlaceList) {
        this.mMyPlaceList = myPlaceList;
        this.mCount = mMyPlaceList.size();
        notifyDataSetChanged();
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlaceViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.place_recycler_view_item, parent, false));
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        holder.placeCardView.setPlace(mMyPlaceList.get(position));
        holder.placeCardView.setListener(mListener);
    }

    @Override
    public int getItemCount() {
        return mCount;
    }
}
