package com.multilemon.test.myplace.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.multilemon.test.myplace.R;
import com.multilemon.test.myplace.model.MyPlace;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by MultiLemon on 1/13/16.
 */
public class PlaceCardView extends CardView implements CompoundButton.OnCheckedChangeListener {

    @Bind(R.id.place_imageview)
    ImageView mPlaceImageView;
    @Bind(R.id.place_name_textview)
    TextView mPlaceNameTextView;
    @Bind(R.id.place_address_textview)
    TextView mPlaceAddressTextView;
    @Bind(R.id.place_url_textview)
    TextView mPlaceUrlTextView;
    @Bind(R.id.place_fav_checkbox)
    CheckBox mPlaceFavCheckbox;

    private MyPlace mMyPlace;
    private PlaceCardViewInterface mListener;

    public interface PlaceCardViewInterface {
        void OnPlaceFavorite(MyPlace myPlace);
        void OnPlaceUnFavorite(MyPlace myPlace);
    }

    public PlaceCardView(Context context) {
        super(context);
        if (!isInEditMode()) {
            init();
        }
    }

    public PlaceCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init();
        }
    }

    public PlaceCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            init();
        }
    }

    private void init() {
        View view = inflate(getContext(), R.layout.place_view, this);
        ButterKnife.bind(this, view);
//        mPlaceFavCheckbox.setOnCheckedChangeListener(this);

    }

    public void setListener(PlaceCardViewInterface listener) {
        this.mListener = listener;
    }

    public void setPlace(MyPlace myPlace) {
        this.mMyPlace = myPlace;
        this.mMyPlace.setView(this);

        mPlaceImageView.setImageBitmap(mMyPlace.getPhoto());
        mPlaceNameTextView.setText(myPlace.getName());
        mPlaceAddressTextView.setText(myPlace.getAddress());
        mPlaceUrlTextView.setText(myPlace.getWebsiteUri() != null ? myPlace.getWebsiteUri().toString() : "");

        mPlaceFavCheckbox.setOnCheckedChangeListener(null);
        mPlaceFavCheckbox.setChecked(myPlace.isFavorite());
        mPlaceFavCheckbox.setOnCheckedChangeListener(this);
    }

    public void setPhoto(Bitmap photo) {
        mPlaceImageView.setImageBitmap(photo);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (mListener != null) {
            if (isChecked) {
                mListener.OnPlaceFavorite(mMyPlace);
            } else {
                mListener.OnPlaceUnFavorite(mMyPlace);
            }
        }
//        this.mMyPlace.setFavorite(isChecked);
    }

}
