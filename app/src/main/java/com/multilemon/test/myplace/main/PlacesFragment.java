package com.multilemon.test.myplace.main;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.multilemon.test.myplace.R;
import com.multilemon.test.myplace.adapter.PlaceAdapter;
import com.multilemon.test.myplace.common.PlaceCardView;
import com.multilemon.test.myplace.model.MyPlace;
import com.multilemon.test.myplace.selectcoordinate.MapsActivity;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacesFragment extends Fragment implements View.OnClickListener,
        ResultCallback<PlaceLikelihoodBuffer>, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "PlacesFragment";
    private static final int COORDINATE_REQUEST_CODE = 1;
    private static final int MAX_RESULT = 10;

    @Bind(R.id.select_coordinate_button)
    Button mSelectCoordinateButton;
    @Bind(R.id.places_recyclerview)
    RecyclerView mPlacesRecyclerView;

    private static PlacesFragment mInstance;
    private static PlaceCardView.PlaceCardViewInterface mListener;
    private PlaceAdapter mAdapter;

    public PlacesFragment() {
        // Required empty public constructor
    }

    public static PlacesFragment newInstance(PlaceCardView.PlaceCardViewInterface listener) {
        mListener = listener;
        if (mInstance == null) {
            mInstance = new PlacesFragment();
        }
        return mInstance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_places, container, false);
        ButterKnife.bind(this, view);

        mSelectCoordinateButton.setOnClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mAdapter = new PlaceAdapter(mListener);

        mPlacesRecyclerView.setHasFixedSize(true);
        mPlacesRecyclerView.setLayoutManager(layoutManager);
        mPlacesRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mSelectCoordinateButton.getId()) {
            Intent intent = new Intent(getContext(), MapsActivity.class);
            startActivityForResult(intent, COORDINATE_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COORDINATE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                double latitude = data.getDoubleExtra(getString(R.string.latitude_double_key), 0d);
                double longitude = data.getDoubleExtra(getString(R.string.longitude_double_key), 0d);
                mSelectCoordinateButton.setText(getString(R.string.latlng_format, latitude, longitude));
                ((MainActivity) getActivity()).searchNearbyPlacesWithinRange(latitude, longitude, 3000);
            }
        }
    }

    public void setDataSet(ArrayList<MyPlace> myPlaceList) {
        if (mAdapter == null) {
            mAdapter = new PlaceAdapter(mListener);
        }
        mAdapter.setPlaceList(myPlaceList);
    }

    public void notifyItemChanged(int position) {
        mAdapter.notifyItemChanged(position);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended: " + i);
    }

    @Override
    public void onResult(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {
//        mAdapter.setPlaceList(placeLikelihoods);
    }

}
