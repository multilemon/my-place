package com.multilemon.test.myplace.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.multilemon.test.myplace.R;
import com.multilemon.test.myplace.common.PlaceCardView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by MultiLemon on 1/13/16.
 */
public class PlaceViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.place_card_view)
    public PlaceCardView placeCardView;

    public PlaceViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
