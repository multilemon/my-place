package com.multilemon.test.myplace.main;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.multilemon.test.myplace.R;
import com.multilemon.test.myplace.adapter.FavoritesPlaceAdapter;
import com.multilemon.test.myplace.common.PlaceCardView;
import com.multilemon.test.myplace.model.MyPlace;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends Fragment {

    private static FavoritesFragment mInstance;
    private static PlaceCardView.PlaceCardViewInterface mListener;

    @Bind(R.id.places_recyclerview)
    RecyclerView mPlacesRecyclerView;

    private FavoritesPlaceAdapter mAdapter;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    public static FavoritesFragment newInstance(PlaceCardView.PlaceCardViewInterface listener) {
        mListener = listener;
        if (mInstance == null) {
            mInstance = new FavoritesFragment();
        }
        return mInstance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mAdapter = new FavoritesPlaceAdapter(mListener);

        mPlacesRecyclerView.setHasFixedSize(true);
        mPlacesRecyclerView.setLayoutManager(layoutManager);
        mPlacesRecyclerView.setAdapter(mAdapter);

        return view;
    }

    public void setDataSet(ArrayList<MyPlace> myPlaceList) {
        if (mAdapter == null) {
            mAdapter = new FavoritesPlaceAdapter(mListener);
        }
        mAdapter.setPlaceList(myPlaceList);
    }

    public void notifyItemChanged(int position) {
        mAdapter.notifyItemChanged(position);
    }

}
